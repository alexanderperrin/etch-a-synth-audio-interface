﻿using UnityEngine;
using System.Collections;

public class InputController : MonoBehaviour {

	public Vector2 input;
	public delegate void InputEventHandler ();
	public event InputEventHandler onActionButtonPressed;

	void Update ()
	{
		input = new Vector2 (Input.GetAxisRaw ("Horizontal"), Input.GetAxisRaw ("Vertical"));

		if (Input.GetKeyDown (KeyCode.Space))
			if (onActionButtonPressed != null)
				onActionButtonPressed ();
	}

	#region SINGLETON

	private static InputController _instance;
	
	public static InputController instance {
		get {
			if (_instance == null) {
				InputController previous = FindObjectOfType (typeof(InputController)) as InputController;
				if (previous) {
					_instance = (InputController)previous;
				} else {
					GameObject go = new GameObject ("__Animator");
					_instance = go.AddComponent<InputController> ();
					go.hideFlags = HideFlags.HideInHierarchy;
				}
			}
			return _instance;
		}
	}

	#endregion
}
