﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ColourButton : MonoBehaviour {

	public MusicLineSystem.LineColor 	color;
	public MusicLineSystem				lineSystem;

	private Image 						image;

	private const float 				MIN_ALPHA = 0.2f;
	private const float 				MAX_ALPHA = 1f;
	private const float 				ALPHA_TRANSITION_SPEED = 0.25f;

	private bool 						active;
	private Button 						button;


	void Awake ()
	{
		image = GetComponent<Image> ();
		button = GetComponent<Button> ();
	}

	void Start ()
	{
		image.CrossFadeAlpha (MIN_ALPHA, 0f, true);
	}

	void OnEnable ()
	{
		lineSystem.onLineColorChanged += LineColourChangedHandler;
		lineSystem.onLineDrawingStopped += LineStoppedHandler;
		lineSystem.onLineDrawingStarted += LineStartedHandler;
	}

	void OnDisable ()
	{
		lineSystem.onLineColorChanged -= LineColourChangedHandler;
		lineSystem.onLineDrawingStopped += LineStoppedHandler;
		lineSystem.onLineDrawingStarted -= LineStartedHandler;
	}

	public void Enable ()
	{
		active = true;
		image.CrossFadeAlpha (MAX_ALPHA, ALPHA_TRANSITION_SPEED, false);
	}

	public void Disable ()
	{
		active = false;
		image.CrossFadeAlpha (MIN_ALPHA, ALPHA_TRANSITION_SPEED, false);
	}

	private void LineColourChangedHandler ()
	{
		if (lineSystem.currentColor != color)
			Disable ();
		else Enable ();
	}

	private void LineStoppedHandler ()
	{
		if (active)
			Disable ();
	}

	private void LineStartedHandler ()
	{
		if (lineSystem.currentColor == color)
			Enable ();
	}

}
