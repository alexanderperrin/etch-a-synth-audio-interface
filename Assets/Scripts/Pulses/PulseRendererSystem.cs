﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PulseRendererSystem : MonoBehaviour {

	public SpriteRenderer rendererPrefab;

	void OnEnable ()
	{
		PulseSystem.onPulseCreated += CreatePulse;
		PulseSystem.onPulseDestroyed += DestroyPulse;
	}

	void OnDisable ()
	{
		PulseSystem.onPulseCreated -= CreatePulse;
		PulseSystem.onPulseDestroyed -= DestroyPulse;
	}

	public void CreatePulse (Pulse p)
	{
		SpriteRenderer r = p.gameObject.AddComponent<SpriteRenderer> ();
		r.sprite = rendererPrefab.sprite;
		r.color = MusicLineSystem.ConvertLineColor (p.m_musicLine.lineColor);
	}

	public void DestroyPulse (Pulse p)
	{
		SpriteRenderer.Destroy (p.GetComponent<SpriteRenderer> ());
	}
}
