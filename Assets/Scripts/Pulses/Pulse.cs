﻿/// <summary>
/// Pulse.
/// Traverses a music line with control from pulse system.
/// Box collider attached to facilitate with trigger events for
/// intersection nodes.
/// </summary>

using UnityEngine;
using System.Collections;

public class Pulse : MonoBehaviour {

	public float 						m_time { get; private set; }
	public Vector2						prevPos;
	public float 						m_traversalSpeed;
	public MusicLine					m_musicLine;
	public BoxCollider2D				_myCollider;
	public BoxCollider2D myCollider {
		get {
			if (_myCollider == null)
			{
				_myCollider = gameObject.AddComponent <BoxCollider2D> ();
			}
			return _myCollider;
		}
		private set {}
	}

	void Start ()
	{
		myCollider.size = Vector2.one * 0.1f;
	}

	void Update ()
	{
		m_time += Time.deltaTime * m_traversalSpeed;
	}

	public void SetPosition (Vector3 position)
	{
		transform.position = position;
	}
}
