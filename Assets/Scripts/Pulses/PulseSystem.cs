﻿/// <summary>
/// Pulse system.
/// Controls the traversal and lifetime of the music line pulses.
/// </summary>

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PulseSystem : MonoBehaviour {

	public MusicLine			m_line;
	public List<Pulse>			m_pulses;
	public float 				m_generationRate = 0.5f;
	public float				m_traversalRate = 1f;
	public Pulse				m_pulsePrefab;
	private int 				m_pulseCount;
	private float				m_lineDistance;
	private const int 			MAX_PULSES = 100;
	private bool				m_created;
	public LayerMask			intersectorsLayerMask;

	public delegate void PulseEventHandler (Pulse p);
	public static event PulseEventHandler onPulseCreated;
	public static event PulseEventHandler onPulseDestroyed;


	void Awake ()
	{
		m_created = false;
		m_pulses = new List<Pulse> (MAX_PULSES);
	}

	void Update ()
	{
		if (m_created)
		{
			Pulse deadPulse = null;
			foreach (Pulse p in m_pulses)
			{
				if (p.m_time > 1f)
					deadPulse = p;
				else
					p.SetPosition (m_line.GetPointOnLine (p.m_time));
			}
			if (deadPulse != null)
				DestroyPulse (deadPulse);
		}
	}

	public void Create (MusicLine line)
	{
		m_line = line;
		m_lineDistance = line.GetManhattanDistance ();
		StartCoroutine (PulseGeneration ());
		m_created = true;
	}

	private IEnumerator PulseGeneration ()
	{
		while (true)
		{
			GeneratePulse ();
			yield return new WaitForSeconds (1f / m_generationRate);
		}
	}

	public void GeneratePulse ()
	{
		++m_pulseCount;
		Pulse p = new GameObject ("Pulse", typeof (Pulse)).GetComponent<Pulse> ();
		p.name = m_line.name + " Pulse " + m_pulseCount.ToString ();
		p.tag = "Player";
		p.m_traversalSpeed = m_traversalRate / m_lineDistance;
		p.m_musicLine = m_line;
		Vector2 pos = m_line.GetPointOnLine (p.m_time);
		p.prevPos = pos;
		p.SetPosition (pos);
		m_pulses.Add (p);
		if (onPulseCreated != null)
			onPulseCreated (p);
	}

	public void DestroyPulse (Pulse pulse)
	{
		if (onPulseDestroyed != null)
			onPulseDestroyed (pulse);
		m_pulses.Remove (pulse);
		GameObject.Destroy (pulse.gameObject);
	}

}
