﻿using UnityEngine;
using System.Collections;

public class NodeParticle : MonoBehaviour {

	public SpriteRenderer spriteRenderer;
	public AnimationCurve scaleOverTime;
	private float time;
	public float timeScale;

	void Start ()
	{
		time = 0f;
	}

	void Update ()
	{
		time += Time.deltaTime * timeScale;
		transform.localScale = Vector3.one * scaleOverTime.Evaluate (time);
		if (time > 1f)
			GameObject.Destroy (gameObject);
	}
}
