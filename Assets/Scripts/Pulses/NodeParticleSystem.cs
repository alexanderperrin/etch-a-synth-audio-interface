﻿using UnityEngine;
using System.Collections;

public class NodeParticleSystem : MonoBehaviour {

	[System.Serializable]
	public class NodeParticlePreset
	{
		public string id;
		public NodeParticle particle;
	}
	public NodeParticlePreset[] presets;

	private static NodeParticleSystem _instance;
	public static NodeParticleSystem instance
	{
		get {
			if (_instance == null)
				_instance = FindObjectOfType<NodeParticleSystem> ();
			return _instance;
		}
	}

	public static void CreateParticle (string id, Vector3 position, Color color)
	{
		for (int i = 0; i < instance.presets.Length; ++i)
		{
			if (instance.presets[i].id == id)
			{
				NodeParticle p = Instantiate (instance.presets[i].particle, position, Quaternion.identity) as NodeParticle;
				p.spriteRenderer.color = color;
			}
		}
	}
}
