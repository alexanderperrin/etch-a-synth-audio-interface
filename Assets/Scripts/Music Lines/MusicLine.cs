﻿/// <summary>
/// Music line.
/// Encapsulates all data regarding the functionality
/// of a music line structure.
/// </summary>

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof (PulseSystem))]
public class MusicLine : MonoBehaviour {

	// Line properties
	public MusicLineSystem.LineColor 	lineColor;
	public List<MusicLineSegment> 		segments;
	public int 							numSegments { get; private set; }
	public List<LineIntersection> 		intersections;
	private List<IntersectionNode>		intersectionNodes;
	private List<CornerNode>			cornerNodes;
	private const int 					MAX_SEGMENTS = 100;
	private Vector2 					m_prevDelta;
	public MusicLineSegment				currentSegment;
	public LayerMask					musicLineLayer;

	// Pulse system properties
	private LineTraversal[]				m_traversal;
	private PulseSystem					m_pulseSystem;
	private Collider2D					m_collider;

	public delegate void OnSegmentAddedHandler ();
	public event OnSegmentAddedHandler onSegmentAdded;

	void Awake ()
	{
		cornerNodes = new List<CornerNode> ();
		intersectionNodes = new List<IntersectionNode> ();
		intersections = new List<LineIntersection> ();
		segments = new List<MusicLineSegment> (MAX_SEGMENTS);
		m_pulseSystem = GetComponent<PulseSystem> ();
	}

	private class LineTraversal
	{
		public Vector3 m_start;
		public Vector3 m_end;
		public float m_startTime;
		public float m_endTime;
		
		public LineTraversal (Vector3 start, Vector3 end, float startTime, float endTime)
		{
			m_start = start;
			m_end = end;
			m_startTime = startTime;
			m_endTime = endTime;
		}
		
		public Vector3 Evalulate (float t)
		{
			float nT = Mathf.InverseLerp (m_startTime, m_endTime, t);
			return Vector3.Lerp (m_start, m_end, nT);
		}
		
		public bool Contains (float t)
		{
			if (t >= m_startTime && t <= m_endTime)
				return true;
			return false;
		}

		public static LineTraversal[] CreateFromLine (MusicLine line)
		{
			float totalDistance = line.GetManhattanDistance ();
			LineTraversal[] traversal = new LineTraversal[line.numSegments];
			float timeShift = 0f;
			for (int i = 0; i < line.segments.Count; ++i)
			{
				float startTime = timeShift;
				timeShift += line.segments[i].GetManhattanDistance () / totalDistance;
				float endTime = timeShift;
				traversal[i] = new LineTraversal (line.segments[i].start,
				                                    line.segments[i].end,
				                                    startTime,
				                                    endTime);
			}
			return traversal;
		}
	}

	public void UpdateLine (Vector2 delta)
	{
		// Determine if the line has changed direction
		float dot = Vector2.Dot (delta.normalized, m_prevDelta.normalized);
		if (Mathf.Approximately (dot, 0f))
		{
			AddLineSegment (currentSegment.end);
		}

		currentSegment.end += delta;
		m_prevDelta = delta;
	}

	public float GetEucledianDistance ()
	{
		float total = 0f;
			foreach (MusicLineSegment segment in segments)
				total += Vector2.Distance (segment.start, segment.end);
		return total;
	}

	public float GetManhattanDistance ()
	{
		float total = 0f;
		foreach (MusicLineSegment segment in segments)
			total += (segment.end - segment.start).magnitude;
		return total;
	}

	public Vector3[] GetPoints ()
	{
		Vector3[] points = new Vector3[segments.Count + 1]; 
		for (int i = 0; i < segments.Count; ++i)
			points[i] = segments[i].start;
		points[points.Length - 1] = segments[points.Length - 2].end;
		return points;
	}

	public void LineDrawingStarted (Vector2 cursorPosition)
	{
		AddLineSegment (cursorPosition);
	}

	public void LineDrawingStopped ()
	{
		Vector3[] p3d = GetPoints ();
		Vector2[] points = new Vector2[p3d.Length];
		for (int i = 0; i < points.Length; ++i)
			points[i] = (Vector2)p3d[i];
		EdgeCollider2D ec2d = gameObject.AddComponent<EdgeCollider2D> ();
		ec2d.points = points;
		m_collider = ec2d;
		m_traversal = LineTraversal.CreateFromLine (this);
		m_pulseSystem.Create (this);
		// Create corners
		// Start from two because of duplicate line segments when line drawing starts
		for (int i = 2; i < points.Length - 1; ++i)
			AddCornerNode (points[i]);
		CheckIntersections (true);
	}

	public void CheckIntersections (bool notifyOthers)
	{
		foreach (MusicLineSegment seg in segments)
		{
			RaycastHit2D[] hit = Physics2D.LinecastAll (seg.start, seg.end, 1 << LayerMask.NameToLayer ("MusicLine"));
			for (int i = 0; i < hit.Length; ++i)
			{
				// Ensure that an intersection is not a corner
				bool corner = false;
				if (cornerNodes != null)
				{
					foreach (CornerNode c in cornerNodes)
					{
						if (c.transform.position == (Vector3)hit[i].point)
							corner = true;
					}
				}
				if (!corner)
				{
					MusicLine m = hit[i].collider.GetComponent<MusicLine> ();
					LineIntersection intersection = new LineIntersection (m, hit[i].point);
					if (!intersections.Contains (intersection))
						AddIntersection (intersection);
					if (notifyOthers)
						m.CheckIntersections (false);
				}
			}
		}
	}

	public Vector3 GetPointOnLine (float t)
	{
		for (int i = 0; i < m_traversal.Length; ++i)
		{
			if (m_traversal[i].Contains (t))
				return m_traversal[i].Evalulate (t);
		}
		return segments[0].start;
	}

	private void AddLineSegment (Vector2 startPos)
	{
		currentSegment = new MusicLineSegment (startPos, startPos);

		if (currentSegment != null)
			segments.Add (currentSegment);

		++numSegments;

		if (onSegmentAdded != null)
			onSegmentAdded ();
	}

	public void AddIntersection (LineIntersection i)
	{
		intersections.Add (i);
		IntersectionNode n = new GameObject ("Intersection Node", typeof (IntersectionNode)).GetComponent<IntersectionNode> ();
		n.transform.position = i.point;
		n.Setup ();
		n.line = this;
		intersectionNodes.Add (n);
	}

	public void RemoveIntersection (LineIntersection i)
	{
		intersections.Remove (i);
	}

	public void AddCornerNode (Vector2 corner)
	{
		CornerNode n = new GameObject ("Corner Node", typeof (CornerNode)).GetComponent<CornerNode> ();
		n.line = this;
		n.transform.position = corner;
		n.Setup ();
		cornerNodes.Add (n);
	}
}

public class MusicLineSegment
{
	public Vector2 start;
	public Vector2 end;
	
	public MusicLineSegment (Vector2 start, Vector2 end)
	{
		this.start = start;
		this.end = end;
	}
	
	public float GetEucledianDistance ()
	{
		return Vector3.Distance (start, end);
	}
	
	public float GetManhattanDistance ()
	{
		return Vector3.Magnitude (end - start);
	}
}

public class LineIntersection
{
	public MusicLine intersectingLine;
	public Vector2 point;
	
	public LineIntersection (MusicLine sect, Vector2 p)
	{
		intersectingLine = sect;
		point = p;
	}
	
	public override bool Equals (object obj)
	{
		if (obj.GetType () != typeof (LineIntersection))
			return false;
		LineIntersection i = (LineIntersection)obj;
		if (i.intersectingLine != this.intersectingLine)
			return false;
		if (i.point != this.point)
			return false;
		return true;
	}
}
