﻿/// <summary>
/// Corner node.
/// Checks for intersections with pulses using triggers.
/// </summary>

using UnityEngine;
using System.Collections;

public class CornerNode : MonoBehaviour {

	public MusicLine line;
	private Rigidbody2D rb;
	private BoxCollider2D _colliderCache;
	public BoxCollider2D colliderCache {
		get {
			if (_colliderCache == null)
				_colliderCache = GetComponent<BoxCollider2D> ();
			return _colliderCache;
		}
		private set {}
	}

	public void Setup ()
	{
		rb = gameObject.AddComponent <Rigidbody2D> ();
		rb.isKinematic = true;
		gameObject.AddComponent <BoxCollider2D> ();
		colliderCache.size = Vector2.one * 0.05f;
		gameObject.layer = LayerMask.NameToLayer ("Intersection");
		colliderCache.isTrigger = true;
	}
	
	void OnTriggerEnter2D (Collider2D other) 
	{
		if (other.tag == "Player")
		{
			Pulse p = other.GetComponent<Pulse> ();
			{
				if (p.m_musicLine == line)
				{
					Color lineColor = MusicLineSystem.ConvertLineColor (line.lineColor);
					NodeParticleSystem.CreateParticle ("Corner", transform.position, lineColor);
				}
			}
		}
	}
}
