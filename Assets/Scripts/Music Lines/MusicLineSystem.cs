﻿/// <summary>
/// Music line system.
/// Top level for music line data structure.
/// Used for creating or 'drawing' the line segments.
/// </summary>

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MusicLineSystem : MonoBehaviour {

	public Transform cursor;

	// Drawing properties
	private bool 				drawingLine;
	private Vector2 			cursorPosition;
	public MusicLine 			currentLine { get; private set; }
	public float 				movementSpeed;
	public LineColor 			currentColor { get; private set; }

	// System properties
	private const int 			MAX_LINES = 500;
	public List<MusicLine> 		musicLines { get; private set; }
	public int 					numLines { get; private set; }

	public delegate void 		LineEventHandler ();
	public event LineEventHandler onLineColorChanged;
	public event LineEventHandler onLineDrawingStarted;
	public event LineEventHandler onLineDrawingStopped;
	public event LineEventHandler onLineDrawingFinished;
	public delegate void 		LineSegmentAddedHandler ();
	public event LineSegmentAddedHandler onLineSegmentAdded;
	public LayerMask			musicLineMask;


	public enum LineColor {
		Red,
		Blue,
		Green,
		Yellow,
		Pink,
		None
	};

	void Awake ()
	{
		musicLines = new List<MusicLine> (MAX_LINES);
	}

	void Start ()
	{
		currentColor = LineColor.Blue;
	}

	void OnEnable ()
	{
		InputController.instance.onActionButtonPressed += ActionButtonPressedHandler;
	}

	void OnDisable ()
	{
		InputController.instance.onActionButtonPressed -= ActionButtonPressedHandler;
	}

	void Update ()
	{
		if (InputController.instance.input.magnitude > 0)
			MoveCursor (InputController.instance.input * Time.deltaTime * movementSpeed);
		Debug.DrawRay (cursorPosition, Vector2.up, Color.red);
	}

	private void MoveCursor (Vector2 movement)
	{
		cursorPosition += movement;
		cursor.position = cursorPosition;
		if (drawingLine)
			currentLine.UpdateLine (movement);
	}

	private void ActionButtonPressedHandler ()
	{
		return;
		if (drawingLine)
			StopDrawingLine ();
		else StartDrawingLine ();
	}

	public void ColourButtonClickedHandler (ColourButton button)
	{
		if (drawingLine)
		{
			// Switch between line colours
			if (currentColor != button.color)
			{
				StopDrawingLine ();
				ChangeColour (button.color);
				StartDrawingLine ();
			}
			// Stop drawing current line
			else
			{
				StopDrawingLine ();
			}
		}
		// Start drawing new line
		else
		{
			if (currentColor != button.color)
				ChangeColour (button.color);
			StartDrawingLine ();
		}
	}

	public void ChangeColour (LineColor color)
	{
		currentColor = color;
		if (onLineColorChanged != null)
			onLineColorChanged ();
	}

	public static Color ConvertLineColor (LineColor color)
	{
		switch (color)
		{
		case LineColor.Blue:
			return Color.blue;
		case LineColor.Green:
			return Color.green;
		case LineColor.Red:
			return Color.red;
		case LineColor.Pink:
			return new Color (1f, 0f, 0.5f);
		case LineColor.Yellow:
			return new Color (1f, 1f, 0f);
		default: return Color.red;
		}
	}

	private void StartDrawingLine ()
	{
		MusicLine line = new GameObject ("Music Line " + numLines.ToString (), typeof (MusicLine)).GetComponent<MusicLine> ();
		currentLine = line;
		line.gameObject.layer = LayerMask.NameToLayer ("MusicLine");
		line.lineColor = currentColor;
		line.LineDrawingStarted (cursorPosition);
		line.musicLineLayer = musicLineMask;
		drawingLine = true;
		currentLine.onSegmentAdded += HandleLineSegmentAdded;
		if (onLineDrawingStarted != null)
			onLineDrawingStarted ();
	}

	private void HandleLineSegmentAdded ()
	{
		if (onLineSegmentAdded != null)
			onLineSegmentAdded ();
	}

	private void StopDrawingLine ()
	{
		drawingLine = false;
		currentLine.LineDrawingStopped ();
		musicLines.Add (currentLine);
		++numLines;
		if (onLineDrawingFinished != null)
			onLineDrawingFinished ();
		currentLine = null;
		currentColor = LineColor.None;
		if (onLineDrawingStopped != null)
			onLineDrawingStopped ();
	}
}
