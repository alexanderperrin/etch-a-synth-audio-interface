﻿/// <summary>
/// Music line renderer.
/// Renders the music lines with vector lines.
/// </summary>

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Vectrosity;

public class MusicLineRenderer : MonoBehaviour {

	public MusicLineSystem 		lineSystem;
	private List<VectorLine> 	vectorLines;
	private Vector3[]			currentPoints;
	private VectorLine			currentLine;
	public Material 			lineMaterial;

	void Awake ()
	{
		vectorLines = new List<VectorLine> ();
	}

	void OnEnable ()
	{
		lineSystem.onLineSegmentAdded += RenderCurrentLine;
		lineSystem.onLineDrawingStarted += StartDrawingLine;
		lineSystem.onLineDrawingFinished += RenderCurrentLine;
		lineSystem.onLineDrawingStopped += StopDrawingLine;
	}

	void OnDisable ()
	{
		lineSystem.onLineSegmentAdded -= RenderCurrentLine;
		lineSystem.onLineDrawingStarted -= StartDrawingLine;
		lineSystem.onLineDrawingFinished -= RenderCurrentLine;
		lineSystem.onLineDrawingStopped -= StopDrawingLine;
	}

	public void StartDrawingLine ()
	{
		currentLine = new VectorLine ("Line", new Vector3[] {Vector3.zero, Vector3.zero}, lineMaterial, 2f, LineType.Continuous);
		currentLine.SetColor (MusicLineSystem.ConvertLineColor (lineSystem.currentColor));
	}

	public void RenderCurrentLine ()
	{
		if (lineSystem.currentLine == null)
			return;
		MusicLine line = lineSystem.currentLine;
		currentPoints = line.GetPoints ();
		currentLine.Resize (currentPoints);
		currentLine.Draw3D ();
	}

	public void StopDrawingLine ()
	{
		currentPoints = null;
		currentLine = null;
	}

	public void UpdateCurrentLine ()
	{
		currentPoints[currentPoints.Length - 1] = lineSystem.currentLine.currentSegment.end;
		currentLine.points3 = currentPoints;
		currentLine.Draw3D ();
	}

	void Update ()
	{
		if (lineSystem.currentLine != null && currentPoints != null)
			UpdateCurrentLine ();
	}
	
}
