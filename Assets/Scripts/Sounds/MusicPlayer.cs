﻿/// <summary>
/// Music Player.
/// Parses intersection events into musical tones.
/// Alexander Perrin 2014
/// </summary>

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MusicPlayer : MonoBehaviour {

	#region INLINE_CLASSES

	[System.Serializable]
	public class Instrument
	{
		public string name;
		public int instrumentType;
		public AudioClip[] notes;
	}

	public class MusicWire
	{
		public Instrument instrument;
		public AudioSource audioSource;

		public void Play (int note)
		{
			audioSource.clip = instrument.notes[note];
			audioSource.Play ();
		}

		public MusicWire (Instrument instrument, AudioSource audioSource)
		{
			this.instrument = instrument;
			this.audioSource = audioSource;
		}
	}

	#endregion

	public Instrument[] 				instruments;
	public Dictionary<int, MusicWire> 	wires = new Dictionary<int, MusicWire> ();
	public AudioSource					audioSourceBase;

	#region UNITY_MONOBEHAVIOUR_MESSAGES

	void OnEnable ()
	{
		// Subscribe to message parse events to play note
		MessageCodeParser.OnMessageParsed += PlayNote;
	}

	void OnDisable ()
	{
		MessageCodeParser.OnMessageParsed -= PlayNote;
	}

	#endregion

	#region PRIVATE_METHODS

	private void OnIntersectionHandler (Pulse pulse, MusicLine line, Vector3 point)
	{
		Debug.DrawRay (point, new Vector3 (1f, 1f) * 0.1f, Color.black, 1f);
	}

	private void PlayNote (int id, int instrumentType, int note)
	{
		if (!wires.ContainsKey (id))
			CreateWire (id, instrumentType);
		wires[id].Play (note);
	}

	private MusicWire CreateWire (int id, int instrumentType)
	{
		AudioSource source = AudioSource.Instantiate (audioSourceBase) as AudioSource;
		MusicWire wire = new MusicWire (GetInstrumentByType (instrumentType), source);
		wires.Add (id, wire);
		return wire;
	}

	private Instrument GetInstrumentByType (int type)
	{
		for (int i = 0; i < instruments.Length; ++i)
		{
			if (instruments[i].instrumentType == type)
				return instruments[i];
		}
		return null;
	}

	#endregion
}
