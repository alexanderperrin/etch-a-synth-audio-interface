﻿/// <summary>
/// Message code parser. Parses midi messages into music note messages
/// for use in MusicPlayer.cs.
/// 
/// Alexander Perrin 2014
/// </summary>

using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;
using System.Collections.Generic;

public class MessageCodeParser : MonoBehaviour {

	#region EVENTS

	public delegate void MessageCodeParseHandler (int id, int instrument, int note);
	public static event MessageCodeParseHandler OnMessageParsed;

	#endregion

	private Queue <MidiBridge.Message> messages;

	void Update ()
	{
		messages = MidiBridge.instance.incomingMessageQueue;

		// Example of values that need to be accepted
//		int n = Random.Range (0, 7);
//		int c = Random.Range (0, 4);
//		int idd = Random.Range (0, 50);
//		ParseMessage (idd.ToString () + " " + c.ToString () + " " + n.ToString () + " ");

		for (int i = 0; i < messages.Count; ++i)
		{
			MidiBridge.Message m = messages.Dequeue ();

			// The note status (ie, note on)
			int cmd = (m.status) & 0x90;
			// The type of the note
			int note = (m.data2);
			// The instrument colour id
			int color = (m.status) & 0x0f;
			// The line id
			int id = (m.data1);
			if (cmd == 0x90) {
//				ParseMessage (id.ToString () + " " + color.ToString () + " " + note.ToString ());
				ParseMessage ("0 " + color + " " + note.ToString ());
			}
		}
	}

	#region PUBLIC_METHODS

	/// <summary>
	/// Parses the message.
	/// </summary>
	/// <param name="message">Should be formatted as "id insturment note" eg "0 3 10"</param>
	public static void ParseMessage (string message)
	{
		string[] tokens = Regex.Split (message, " ");

		int id = -1;
		int instrument = -1;
		int note = -1;

		bool success = true;
		if (!int.TryParse (tokens[0], out id))
		{
			Debug.LogError ("Error parsing id code.");
			success = false;
		}
		if (!int.TryParse (tokens[1], out instrument))
		{
			Debug.LogError ("Error parsing instrument code.");
			success = false;
		}
		if (!int.TryParse (tokens[2], out note))
		{
			Debug.LogError ("Error parsing note code.");
			success = false;
		}

		if (success && OnMessageParsed != null)
			OnMessageParsed (id, instrument, note);
	}

	#endregion
}
